package com.demo.microservices.account.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.demo.microservices.account.model.Account;
import com.demo.microservices.account.model.AccountTransfer;
import com.demo.microservices.account.service.AccountService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value = "Accounts V2")
@RestController
public class AccountController {

	private static final String TEST_CIRCUIT_BREAKER = "testCircuitBreaker";
	
	@Autowired
	AccountService accountService;
	
	@Value("${bootcamp.product.server}")
	private String productServer;
	
	@Value("${bootcamp.product.port}")
	private String productPort;
	 
	@ApiOperation(value="포트폴리오 목록 조회 입니다.")
	@GetMapping("/accounts")
	public ResponseEntity<List<Account>> getAccountList() {
		List<Account> accountList = accountService.getAccountAll();
		log.info("getAccountList:{}", "call ");
		
		return new ResponseEntity<>(accountList, HttpStatus.OK);
	}
	
	@ApiOperation(value="폴트폴리오를 조회  되었습니다. ")
	@GetMapping("/accounts/{accountNo}")
	public ResponseEntity<Account> getAccount(@PathVariable String accountNo) {
		
		Account account = accountService.getAccount(accountNo);
		
		return new ResponseEntity<>(account, HttpStatus.OK);
	}
	
	@ApiOperation(value="포트폴리오 등록입니다.")
	@PostMapping("/accounts")
	public ResponseEntity<String> addAccount(@RequestBody Account account) {
		String msg = "";
		
		int rc = accountService.addAccount(account);
		
		log.info("rc:{}", rc);
		
		return new ResponseEntity<>("정상 처리되었습니다.", HttpStatus.OK);
	}
	
	@ApiOperation(value="폴트폴리오가 수정 되었습니다. ")
	@PutMapping("/accounts/{accountNo}")
	public ResponseEntity<String> saveAccount(@PathVariable String accountNo, @RequestBody Account acc) {
		
		acc.setAccNo(accountNo);
		
		int rc = accountService.updateAccount(acc);
		
		return new ResponseEntity<>("포트폴리오가 수정이 정상 처리되었습니다.", HttpStatus.OK);
	}
	
	@ApiOperation(value="폴트폴리오가 삭제 되었습니다. ")
	@DeleteMapping("/accounts/{accountNo}")
	public ResponseEntity<String> deleteAccount(@PathVariable String accountNo) {
		
		int rc = accountService.deleteAccount(accountNo);
		
		return new ResponseEntity<>("삭제가 정상 처리되었습니다.", HttpStatus.OK);
	}
	
	@ApiOperation(value="상품 조회가 정창 처리되었습니다. ")
	@GetMapping("/accounts/products")
	public ResponseEntity<Object> getProduct() {
		
		String uri = String.format("http://%s:%s/products", productServer, productPort);
		
		Object products = null;

		RestTemplate rest = new RestTemplate();
//		Object products = null;
		
		try {
			log.info("call Product REST API");
			
			products = rest.exchange(uri, HttpMethod.GET, null, Object.class).getBody();
			 
			log.info("call Product REST API :{}", products);
		} catch (Exception e) {
			log.error("ERROR", e);
			throw new RuntimeException(e);
		}
		log.info("success get Products");
		
		
		return new ResponseEntity<Object>(products, HttpStatus.OK);
	}
	
	@ApiOperation(value="계좌이체 서비스입니다. ")
	@PostMapping("/account/transfer")
	public ResponseEntity<String> transfer(@RequestBody AccountTransfer accountTransfer) {
		try {
			accountService.transfer(accountTransfer);
		} catch (Exception e) {
			log.error("ERROR", e);
			throw new RuntimeException(e);
		}
		
		return new ResponseEntity<String>("정상 처리되었습니다.",HttpStatus.OK);
	}
	
}
