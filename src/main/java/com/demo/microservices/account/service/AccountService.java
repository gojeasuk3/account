package com.demo.microservices.account.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.microservices.account.dao.CommonDao;
import com.demo.microservices.account.model.Account;
import com.demo.microservices.account.model.AccountTransfer;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AccountService {

	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	
	public List<Account> getAccountAll() {
    	try {
            List<Account> accounts =  commonDao.selectList("selectAccAll");
            
            return accounts;
    	} catch (Exception e) {
    		throw new RuntimeException(e);
    	}
	}
	
	public Account getAccount(String accountNo) {
    	try {
            Account account =  commonDao.selectOne("findByAccNo", accountNo);
            
            return account;
    	} catch (Exception e) {
    		throw new RuntimeException(e);
    	}
	}
	
	public int addAccount(Account account) {
    	int rc = 0;
    	try {
            rc = commonDao.insert("addAccount", account);
            
            return rc;
    	} catch (Exception e) {
    		log.error("[ERROR]", e);
    		
    		throw new RuntimeException(e);
    	}
	}
	
	public int deleteAccount(String accountNo) {
    	int rc = 0;
    	try {
            rc = commonDao.insert("deleteAccount", accountNo);
            
            return rc;
    	} catch (Exception e) {
    		log.error("[ERROR]", e);
    		
    		throw new RuntimeException(e);
    	}
	}
	
	public int updateAccount(Account account) {
    	int rc = 0;
    	try {
            rc = commonDao.update("updateAccount", account);
            
            return rc;
    	} catch (Exception e) {
    		log.error("[ERROR]", e);
    		
    		throw new RuntimeException(e);
    	}
	}
	
	@KafkaListener(topics = "${bootcamp.account.topic}")
	@Transactional
	public void receiveAccount(String message, Acknowledgment ack) {
		Gson gson = new Gson();
		
		try {
			log.info("from message:{}", message);
			
			AccountTransfer	acc = gson.fromJson(message, AccountTransfer.class);
			log.info("account object from kafka:{}", acc);
			
			ack.acknowledge();
		}
		
		catch (Exception e) {
			log.error("ERROR", e);
			throw new RuntimeException(e);
		}
	}
	
	public int transfer(AccountTransfer transfer) {
		Gson gson = new Gson();
		log.info("input account:{}", transfer);
		
		try {
			String topic = commonDao.selectOne("findByTopic", transfer.getBank());
			log.info("input topic:{}", topic);
			
			String message = gson.toJson(transfer);
			
			commonDao.update("transfer", transfer);
			kafkaTemplate.send(topic, message);
		}
		
		catch (Exception e) {
			log.error("ERROR", e);
			throw new RuntimeException(e);
		}
		return 0;
	}
}
